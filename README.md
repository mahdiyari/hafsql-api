# HafSQL API
Provides API access to the tables prepared by the [HafSQL](https://gitlab.com/mahdiyari/hafsql) and some other tables from the HAF database.

## Table of contents
- [Installation](#installation)
- [APIs](#apis)
- [The call structure](#the-call-structure)
- [Operations](#operations)
- [Extra methods](#extra-methods)
- [Custom methods](#custom-methods)
- [Development](#development)
- [Limitations](#limitations)
- [License](#license)
  
  
### Installation 
Requirements:  
- Install and sync [HafSQL](https://gitlab.com/mahdiyari/hafsql)  
- On the same machine as HafSQL  
  
```bash
npm install
```
  
You can create `.env` file and edit the port and such:
```bash
cp example.env .env
```
Note: `PGPOOLSIZE` is per instance. By default there are 4 instances so 2x4 = 8 Postgresql connections are used for API.  
  
To start:  
```bash
npm run start
```
By default 4 instances of the API are launched in cluster mode. You can change this in `ecosystem.config.cjs`.  
  

See logs:  
```bash
npm run logs
```
  
List apps:  
```bash
npm run list
```
  
To stop:  
```bash
npm run stop
```
  
To restart:
```bash
npm run restart
```

### Docker installation

```
cp example.env .env
```

Building:
```bash
docker build -t hafsql-api-v1.0.0 .
```

Running:
```bash
docker run --rm -it -p 9090:9090 --name hafsql-api hafsql-api-v1.0.0
```  
  
It will run 4 instances of the API inside one docker container. (cluster mode - configurable in ecosystem.config.cjs)  
  
  
***
## APIs

There are 3 general types of APIs.  
- Operations
- - Raw operations data with prefix `op_` and `vo_`
- Extra
- - The data that is synced by HafSQL e.g. `delegations` and `comments` - No prefix
- Custom queries
- - Custom queries that are added for the community needs with prefix `c_`

***
### The call structure
The API is compatible with jussi so it is JSON-RPC2  
```json
{"jsonrpc":"2.0", "method":"hafsql.op_vote", "params":{"author": "mahdiyari", "limit": 1}, "id":1}
```
  
Note: `params` is always json and `method` always has prefix `hafsql.`  
  
General `params`:  
- `limit` integer 1 to 100 - default 10
- `start` integer - `op_id` or `id` used for pagination
- `reverse` boolean - reverse the sorting of data - default false
  

  
***
### Operations
The general structure of the method names:  
`op_` + operation name  
or  
`vo_` + operation name  
  
example:  
```bash
curl -s --data '{"jsonrpc":"2.0", "method":"hafsql.op_vote", "params":{"author": "mahdiyari", "limit": 1}, "id":1}' http://127.0.0.1:9090
```
  
**--> [List of all the operation methods](/docs/Operations.md)**  
  
### Extra methods
  
example:  
```bash
curl -s --data '{"jsonrpc":"2.0", "method":"hafsql.delegations", "params":{"delegatee": "gtg", "limit": 1}, "id":1}' http://127.0.0.1:9090
```
  
**--> [List of all the extra methods](/docs/Extra.md)**  

### Custom methods
Custom queries for the community needs. Open an issue for custom API requests.
  
example:  
```bash
curl -s --data '{"jsonrpc":"2.0", "method":"hafsql.c_comments_by_payout", "params":{"remaining_till_cashout": "1 hours", "limit": 1}, "id":1}' http://127.0.0.1:9090
```
  
**--> [List of all the custom methods](/docs/Custom.md)**  
  

***
### Development
The methods are seperated in 3 files in `src/methods/` and new methods can be added to the end of each array of methods with their related parameters and validators.  
This is straight-forward for the operations and extra methods but for the custom methods, the custom query is placed in `src/helpers/custom_queries/` and then imported in the `src/customQueryHandler.js` file.  
  

### Limitations
Currently it is not possible to send multiple requests in one call as it is supported by the JSON-RPC2 standard. The following is not supported yet:
```json
[
  {"jsonrpc":"2.0", "method":"hafsql.op_vote", "params":{"author": "mahdiyari", "limit": 1}, "id":1},
  {"jsonrpc":"2.0", "method":"hafsql.op_vote", "params":{"author": "gtg", "limit": 1}, "id":2}
]
```

### License
MIT

