module.exports = {
  apps: [{
    name: 'HafSQL-api',
    script: 'src/main.js',
    instances: 4,
    exec_mode: 'cluster'
  }]
}
