FROM node:18

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm ci --omit=dev
COPY . .

RUN npm install pm2 -g

EXPOSE 9090

ENV HOST "0.0.0.0"

CMD [ "pm2-runtime", "start", "ecosystem.config.cjs" ]
