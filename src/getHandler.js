import { Router } from 'express'

export const getHandler = Router()

getHandler.get('*', (req, res) => {
  res.json({
    app: 'HafSQL',
    api: 'JSON-RPC 2.0'
  })
})
