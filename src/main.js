import express from 'express'
import hpp from 'hpp'
import helmet from 'helmet'
import { config as dotenvConfig } from 'dotenv'
import bodyParser from 'body-parser'
import { validateJSONRPC2 } from './helpers/validators/validateJSONRPC2.js'
import { errorObject } from './helpers/errorObject.js'
import { getHandler } from './getHandler.js'
import { rpcHandler } from './rpcHandler.js'
const app = express()
app.use(helmet())
// more info: www.npmjs.com/package/hpp
app.use(hpp())

// support json encoded bodies and encoded bodies
app.use(bodyParser.json({
  type (req) {
    return true
  }
}))
app.use(bodyParser.urlencoded({ extended: true }))

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  )
  next()
})

app.use(validateJSONRPC2)

// APIs are added below this line

app.use('', getHandler)
app.use('', rpcHandler)

// APIs are added above this line
// Error handler
app.use((err, req, res, next) => {
  res.json(errorObject(-32700, 'Parse error', null, err))
})

dotenvConfig()
const port = process.env.PORT || 9090
const host = process.env.HOST || '127.0.0.1'
app.listen(port, host, () => {
  console.log(`Application started on ${host}:${port}`)
})
