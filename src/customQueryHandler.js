import { commentsByPayout } from './helpers/custom_queries/commentsByPayout.js'
import { errorObject } from './helpers/errorObject.js'

export const customQueryHandler = async (res, params, methodParams, callParams, id, limit) => {
  // const data = req.body
  let raw = false
  if (callParams.length < 1) {
    raw = true
  }
  if (methodParams.name === 'c_comments_by_payout') {
    const result = await commentsByPayout(raw, params, callParams, limit)
    res.json({
      jsonrpc: '2.0',
      result: result.rows,
      id
    })
    return
  }
  res.json(errorObject(-32602, 'Invalid params', id))
}
