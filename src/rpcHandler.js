import { Router } from 'express'
import { methodValidator } from './helpers/validators/methodValidator.js'
import { errorObject } from './helpers/errorObject.js'
import { pool } from './helpers/database.js'
import { startValidator } from './helpers/validators/startValidator.js'
import { customQueryHandler } from './customQueryHandler.js'
import { DEFAULT_LIMIT, limitValidator } from './helpers/validators/limitValidator.js'
import { extraHandler } from './extraHandler.js'

export const rpcHandler = Router()

rpcHandler.post('*', async (req, res) => {
  const data = req.body
  let params = {}
  if (Object.hasOwn(data, 'params') && data.params !== null) {
    params = data.params
  }
  const { method, id } = data
  const methodParams = methodValidator(method)
  if (!methodParams) {
    res.json(errorObject(-32600, 'Invalid Request', id))
    return
  }
  const paramKeys = Object.keys(params)
  let raw = false
  let valid = true
  let ah = false
  let ahParam = {}
  const callParams = {}
  if (paramKeys.length < 1) {
    raw = true
  } else {
    // validate params
    const filters = methodParams.filters
    for (let i = 0; i < paramKeys.length; i++) {
      let valid2 = false
      if (paramKeys[i] === 'start' || paramKeys[i] === 'limit' || paramKeys[i] === 'reverse') {
        valid2 = true
        continue
      }
      for (let k = 0; k < filters.length; k++) {
        if (filters[k].param === paramKeys[i]) {
          valid2 = true
          callParams[paramKeys[i]] = params[paramKeys[i]]
          if (filters[k].requires && paramKeys.indexOf(filters[k].requires) < 0) {
            valid = false
            break
          }
          if (!filters[k].validator(params[paramKeys[i]])) {
            valid = false
            break
          }
          if (filters[k].type && filters[k].type === 'ah') {
            ah = true
            ahParam = { param: paramKeys[i], account: params[paramKeys[i]] }
          }
        }
      }
      if (!valid2) {
        valid = false
        break
      }
    }
  }
  if (!valid) {
    res.json(errorObject(-32602, 'Invalid params', id))
    return
  }
  if (Object.keys(callParams).length < 1) {
    raw = true
  } else {
    raw = false
  }
  let limit = DEFAULT_LIMIT
  if (Object.hasOwn(params, 'limit') && limitValidator(params.limit)) {
    limit = params.limit
  }
  // Redirect to custom queriy handler
  if (method.split('.')[1].startsWith('c_')) {
    return customQueryHandler(res, params, methodParams, callParams, id, limit)
  }
  let extra = false
  const splittedMethod = method.split('.')
  if (!splittedMethod[1].startsWith('op_') && !splittedMethod[1].startsWith('vo_')) {
    extra = true
  }
  let start = 0
  let startParam = false
  if (Object.hasOwn(params, 'start') && startValidator(params.start)) {
    start = params.start
    startParam = true
  } else if (ah) {
    start = await getStart(ahParam.account)
  } else {
    start = await getStart()
  }
  let reverse = false
  if (Object.hasOwn(params, 'reverse') && typeof params.reverse === 'boolean') {
    reverse = params.reverse
  }
  if (ah) {
    // Get data from AH table
    const query = `SELECT a.*, ao.account_op_seq_no FROM hive.account_operations ao
      left join (select * from hafsql."${splittedMethod[1]}") a on(ao.operation_id = a.op_id)
      WHERE op_type_id = $1
      and account_id = (SELECT id FROM hive.accounts WHERE name=$2)
      and a."${ahParam.param}" = $2
      and ao.account_op_seq_no ${reverse && startParam ? '>=' : '<='} $3
      order by account_op_seq_no ${reverse ? 'ASC' : 'DESC'}
      limit $4`
    const result = await pool.query(query, [methodParams.id, ahParam.account, start, limit])
    res.json({
      jsonrpc: '2.0',
      result: result.rows,
      id
    })
    return
  }
  if (raw && !extra) {
    const query = `SELECT * FROM hafsql."${splittedMethod[1]}" WHERE op_id ${reverse && startParam ? '>=' : '<='} $1 ORDER BY op_id ${reverse ? 'ASC' : 'DESC'} LIMIT $2`
    const result = await pool.query(query, [start, limit])
    res.json({
      jsonrpc: '2.0',
      result: result.rows,
      id
    })
    return
  } else if (!raw && !extra) {
    let queryString = ''
    const queryParams = []
    const callKeys = Object.keys(callParams)
    for (let i = 0; i < callKeys.length; i++) {
      queryString += `"${callKeys[i]}"=$${i + 1}`
      queryString += ' AND '
      queryParams.push(params[callKeys[i]])
    }
    const query = `SELECT * FROM hafsql."${splittedMethod[1]}" WHERE ${queryString} op_id ${reverse && startParam ? '>=' : '<='} ${start} ORDER BY op_id ${reverse ? 'ASC' : 'DESC'} LIMIT ${limit}`
    const result = await pool.query(query, queryParams)
    res.json({
      jsonrpc: '2.0',
      result: result.rows,
      id
    })
    return
  } else if (extra) {
    // Redirect to extra query handler
    return extraHandler(res, params, methodParams, callParams, id, limit, start, startParam, reverse)
  }
  res.json(errorObject(-32602, 'Invalid params', id))
})

let lastGet = 0
let startCache = 0
const getStart = async (username) => {
  if (username) {
    const result = await pool.query(`SELECT account_op_seq_no FROM hive.account_operations
      WHERE account_id = (SELECT id FROM hive.accounts WHERE name=$1)
      ORDER BY account_op_seq_no DESC LIMIT 1`, [username])
    return result.rows[0]?.account_op_seq_no || 0
  }
  const now = Date.now()
  if (now - lastGet > 1500) {
    lastGet = now
    const result = await pool.query('SELECT id FROM hive.operations ORDER BY id DESC LIMIT 1')
    startCache = result.rows[0].id
    return startCache
  } else {
    return startCache
  }
}
