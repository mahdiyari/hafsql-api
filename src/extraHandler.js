import { pool } from './helpers/database.js'
import { accountsHandler } from './helpers/extra/accounts.js'
import { blocksHandler } from './helpers/extra/blocks.js'
import { commentHandler } from './helpers/extra/comment.js'
import { blacklistsHandler } from './helpers/extra/blacklists.js'
import { operationsHandler } from './helpers/extra/operations.js'
import { transactionHandler } from './helpers/extra/transaction.js'
import { mutesHandler } from './helpers/extra/mutes.js'
import { blacklistFollowsHandler } from './helpers/extra/blacklistFollows.js'
import { muteFollowsHandler } from './helpers/extra/muteFollows.js'
import { followsHandler } from './helpers/extra/follows.js'
import { reblogsHandler } from './helpers/extra/reblogs.js'
import { communitySubsHandler } from './helpers/extra/communitySubs.js'
import { communityRolesHandler } from './helpers/extra/communityRoles.js'

export const extraHandler = async (res, params, methodParams, callParams, id, limit, start, startParam, reverse) => {
  let raw = false
  if (Object.keys(callParams).length < 1) {
    raw = true
  }
  let result = { rows: [] }
  let done = false
  if (methodParams.name === 'transactions') {
    result = await transactionHandler(callParams, limit)
    done = true
  }
  if (methodParams.name === 'comments') {
    result = await commentHandler(callParams, limit, start, reverse, startParam)
    done = true
  }
  if (methodParams.name === 'dynamic_global_properties') {
    if (raw) {
      result = await pool.query(`SELECT * FROM hafsql."${methodParams.name}" ORDER BY block_num DESC LIMIT $1`, [limit])
      done = true
    }
    // !raw can be handled below
  }
  if (methodParams.name === 'accounts') {
    result = await accountsHandler(callParams, limit, start, reverse, startParam)
    done = true
  }
  if (methodParams.name === 'blocks') {
    result = await blocksHandler(callParams, limit, start, reverse, startParam)
    done = true
  }
  if (methodParams.name === 'operations') {
    result = await operationsHandler(callParams, limit, start, reverse, startParam)
    done = true
  }
  if (methodParams.name === 'blacklists') {
    result = await blacklistsHandler(callParams, limit)
    done = true
  }
  if (methodParams.name === 'mutes') {
    result = await mutesHandler(callParams, limit)
    done = true
  }
  if (methodParams.name === 'blacklist_follows') {
    result = await blacklistFollowsHandler(callParams, limit)
    done = true
  }
  if (methodParams.name === 'mute_follows') {
    result = await muteFollowsHandler(callParams, limit)
    done = true
  }
  if (methodParams.name === 'follows') {
    result = await followsHandler(callParams, limit)
    done = true
  }
  if (methodParams.name === 'reblogs') {
    result = await reblogsHandler(callParams, limit)
    done = true
  }
  if (methodParams.name === 'community_subs') {
    result = await communitySubsHandler(callParams, limit)
    done = true
  }
  if (methodParams.name === 'community_roles') {
    result = await communityRolesHandler(callParams, limit)
    done = true
  }

  if (done) {
    res.json({
      jsonrpc: '2.0',
      result: result.rows,
      id
    })
    return
  }

  // handle rest of the methods
  if (raw) {
    const query = `SELECT * FROM hafsql."${methodParams.name}" LIMIT $1`
    const result = await pool.query(query, [limit])
    res.json({
      jsonrpc: '2.0',
      result: result.rows,
      id
    })
  } else {
    let queryString = ''
    const queryParams = []
    const callKeys = Object.keys(callParams)
    for (let i = 0; i < callKeys.length; i++) {
      queryString += `"${callKeys[i]}"=$${i + 1}`
      if (i < callKeys.length - 1) {
        queryString += ' AND '
      }
      queryParams.push(params[callKeys[i]])
    }
    const query = `SELECT * FROM hafsql."${methodParams.name}" WHERE ${queryString}`
    const result = await pool.query(query, queryParams)
    res.json({
      jsonrpc: '2.0',
      result: result.rows,
      id
    })
  }
}
