import { pool } from './database.js'

export const getUserId = async (username) => {
  const result = await pool.query('SELECT id FROM hafsql.accounts WHERE name=$1', [username])
  return result.rows[0]?.id || null
}
