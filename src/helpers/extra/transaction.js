import { pool } from '../database.js'

export const transactionHandler = async (params, limit) => {
  let queryString = ''
  const queryParams = []
  let raw = false
  if (Object.hasOwn(params, 'trx_id')) {
    queryString = 'WHERE x.trx_id=decode($1, \'hex\')'
    queryParams.push(params.trx_id)
  } else if (Object.hasOwn(params, 'block_num')) {
    queryString = 'WHERE x.block_num=$1'
    queryParams.push(params.block_num)
  } else {
    raw = true
  }
  let query = `SELECT x.block_num, x.trx_in_block, encode(x.trx_id, 'hex') AS trx_id, x.ref_block_num, x.ref_block_prefix, x.expiration, x.signatures
      FROM hafsql.transactions x ${queryString}`
  if (raw) {
    query = `SELECT x.block_num, x.trx_in_block, encode(x.trx_id, 'hex') AS trx_id, x.ref_block_num, x.ref_block_prefix, x.expiration, x.signatures
      FROM hafsql.transactions x ORDER BY x.block_num DESC LIMIT $1`
    queryParams.push(limit)
  }
  return pool.query(query, queryParams)
}
