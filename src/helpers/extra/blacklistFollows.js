import { pool } from '../database.js'
import { getUserId } from '../getUserId.js'

export const blacklistFollowsHandler = async (params, limit) => {
  let queryString = ''
  const queryParams = []
  let raw = false
  let i = 1
  if (Object.hasOwn(params, 'account')) {
    const id = await getUserId(params.account)
    if (!id) {
      return { rows: [] }
    }
    queryString += `x.account_id=$${i}`
    queryParams.push(id)
    i++
  }
  if (Object.hasOwn(params, 'blacklist')) {
    const id = await getUserId(params.blacklist)
    if (!id) {
      return { rows: [] }
    }
    if (i > 1) {
      queryString += ' AND '
    }
    queryString += `x.blacklist_id=$${i}`
    queryParams.push(id)
  }
  if (queryString.length < 1) {
    raw = true
  }
  let query = `SELECT x.account_name AS account, x.blacklist_name AS blacklist FROM hafsql.blacklist_follows x WHERE ${queryString}`
  if (raw) {
    query = 'SELECT x.account_name AS account, x.blacklist_name AS blacklist FROM hafsql.blacklist_follows x LIMIT $1'
    queryParams.push(limit)
  }
  return pool.query(query, queryParams)
}
