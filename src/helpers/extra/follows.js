import { pool } from '../database.js'
import { getUserId } from '../getUserId.js'

export const followsHandler = async (params, limit) => {
  let queryString = ''
  const queryParams = []
  let raw = false
  let i = 1
  if (Object.hasOwn(params, 'follower')) {
    const id = await getUserId(params.follower)
    if (!id) {
      return { rows: [] }
    }
    queryString += `x.follower_id=$${i}`
    queryParams.push(id)
    i++
  }
  if (Object.hasOwn(params, 'following')) {
    const id = await getUserId(params.following)
    if (!id) {
      return { rows: [] }
    }
    if (i > 1) {
      queryString += ' AND '
    }
    queryString += `x.following_id=$${i}`
    queryParams.push(id)
  }
  if (queryString.length < 1) {
    raw = true
  }
  let query = `SELECT x.follower_name AS follower, x.following_name AS following FROM hafsql.follows x WHERE ${queryString}`
  if (raw) {
    query = 'SELECT  x.follower_name AS follower, x.following_name AS following FROM hafsql.follows x LIMIT $1'
    queryParams.push(limit)
  }
  return pool.query(query, queryParams)
}
