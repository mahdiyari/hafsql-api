import { pool } from '../database.js'
import { getUserId } from '../getUserId.js'

export const mutesHandler = async (params, limit) => {
  let queryString = ''
  const queryParams = []
  let raw = false
  let i = 1
  if (Object.hasOwn(params, 'muter')) {
    const id = await getUserId(params.muter)
    if (!id) {
      return { rows: [] }
    }
    queryString += `x.muter_id=$${i}`
    queryParams.push(id)
    i++
  }
  if (Object.hasOwn(params, 'muted')) {
    const id = await getUserId(params.muted)
    if (!id) {
      return { rows: [] }
    }
    if (i > 1) {
      queryString += ' AND '
    }
    queryString += `x.muted_id=$${i}`
    queryParams.push(id)
  }
  if (queryString.length < 1) {
    raw = true
  }
  let query = `SELECT x.muter_name AS muter, x.muted_name AS muted FROM hafsql.mutes x WHERE ${queryString}`
  if (raw) {
    query = 'SELECT x.muter_name AS muter, x.muted_name AS muted FROM hafsql.mutes x LIMIT $1'
    queryParams.push(limit)
  }
  return pool.query(query, queryParams)
}
