import { pool } from '../database.js'
import { getUserId } from '../getUserId.js'

export const communitySubsHandler = async (params, limit) => {
  let queryString = ''
  const queryParams = []
  let raw = false
  let i = 1
  if (Object.hasOwn(params, 'account')) {
    const id = await getUserId(params.account)
    if (!id) {
      return { rows: [] }
    }
    queryString += `x.account_id=$${i}`
    queryParams.push(id)
    i++
  }
  if (Object.hasOwn(params, 'community')) {
    const id = await getUserId(params.community)
    if (!id) {
      return { rows: [] }
    }
    if (i > 1) {
      queryString += ' AND '
    }
    queryString += `x.community_id=$${i}`
    queryParams.push(id)
  }
  if (queryString.length < 1) {
    raw = true
  }
  let query = `SELECT x.account_name AS account, x.community_name AS community FROM hafsql.community_subs x WHERE ${queryString}`
  if (raw) {
    query = 'SELECT  x.account_name AS account, x.community_name AS community FROM hafsql.community_subs x LIMIT $1'
    queryParams.push(limit)
  }
  return pool.query(query, queryParams)
}
