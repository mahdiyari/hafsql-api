import { pool } from '../database.js'

export const accountsHandler = async (params, limit, start, reverse, startParam) => {
  if (!startParam) {
    start = await getStart()
  }
  let queryString = ''
  const queryParams = []
  const paramKeys = Object.keys(params)
  for (let i = 0; i < paramKeys.length; i++) {
    queryString += `"${paramKeys[i]}"=$${i + 1}`
    queryString += ' AND '
    queryParams.push(params[paramKeys[i]])
  }
  const query = `SELECT x.* FROM hafsql.accounts x WHERE ${queryString} id ${reverse && startParam ? '>=' : '<='} ${start}
    ORDER BY id ${reverse ? 'ASC' : 'DESC'} LIMIT ${limit}`
  return pool.query(query, queryParams)
}

let lastGet = 0
let startCache = 0
const getStart = async () => {
  const now = Date.now()
  if (now - lastGet > 1500) {
    lastGet = now
    const result = await pool.query('SELECT id FROM hafsql.accounts ORDER BY id DESC LIMIT 1')
    startCache = result.rows[0].id
    return startCache
  } else {
    return startCache
  }
}
