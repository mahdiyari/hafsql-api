import { pool } from '../database.js'
import { getUserId } from '../getUserId.js'

export const reblogsHandler = async (params, limit) => {
  let queryString = ''
  const queryParams = []
  let raw = false
  let i = 1
  if (Object.hasOwn(params, 'account')) {
    const id = await getUserId(params.account)
    if (!id) {
      return { rows: [] }
    }
    queryString += `x.account_id=$${i}`
    queryParams.push(id)
    i++
  }
  if (Object.hasOwn(params, 'author') && Object.hasOwn(params, 'permlink')) {
    const id = await getPostId(params.author, params.permlink)
    if (!id) {
      return { rows: [] }
    }
    if (i > 1) {
      queryString += ' AND '
    }
    queryString += `x.post_id=$${i}`
    queryParams.push(id)
  }
  if (queryString.length < 1) {
    raw = true
  }
  let query = `SELECT x.account_name AS account,
    (SELECT author FROM hafsql.comments WHERE id=x.post_id) AS author,
    (SELECT permlink FROM hafsql.comments WHERE id=x.post_id) AS permlink
    FROM hafsql.reblogs x WHERE ${queryString}`
  if (raw) {
    query = `SELECT x.account_name AS account,
      (SELECT author FROM hafsql.comments WHERE id=x.post_id) AS author,
      (SELECT permlink FROM hafsql.comments WHERE id=x.post_id) AS permlink
      FROM hafsql.reblogs x LIMIT $1`
    queryParams.push(limit)
  }
  return pool.query(query, queryParams)
}

const getPostId = async (author, permlink) => {
  const result = await pool.query('SELECT id FROM hafsql.comments WHERE author=$1 AND permlink=$2', [author, permlink])
  return result.rows[0]?.id || null
}
