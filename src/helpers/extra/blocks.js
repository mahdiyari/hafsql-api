import { pool } from '../database.js'

export const blocksHandler = async (params, limit, start, reverse, startParam) => {
  if (!startParam) {
    start = await getStart()
  }
  let queryString = ''
  const queryParams = []
  const paramKeys = Object.keys(params)
  for (let i = 0; i < paramKeys.length; i++) {
    queryString += `"${paramKeys[i]}"=$${i + 1}`
    queryString += ' AND '
    queryParams.push(params[paramKeys[i]])
  }
  const query = `SELECT x.* FROM hafsql.blocks x WHERE ${queryString} block_num ${reverse && startParam ? '>=' : '<='} ${start}
    ORDER BY block_num ${reverse ? 'ASC' : 'DESC'} LIMIT ${limit}`
  return pool.query(query, queryParams)
}

let lastGet = 0
let startCache = 0
const getStart = async () => {
  const now = Date.now()
  if (now - lastGet > 1500) {
    lastGet = now
    const result = await pool.query('SELECT block_num FROM hafsql.blocks ORDER BY block_num DESC LIMIT 1')
    startCache = result.rows[0].block_num
    return startCache
  } else {
    return startCache
  }
}
