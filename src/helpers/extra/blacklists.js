import { pool } from '../database.js'
import { getUserId } from '../getUserId.js'

export const blacklistsHandler = async (params, limit) => {
  let queryString = ''
  const queryParams = []
  let raw = false
  let i = 1
  if (Object.hasOwn(params, 'blacklister')) {
    const id = await getUserId(params.blacklister)
    if (!id) {
      return { rows: [] }
    }
    queryString += `x.blacklister_id=$${i}`
    queryParams.push(id)
    i++
  }
  if (Object.hasOwn(params, 'blacklisted')) {
    const id = await getUserId(params.blacklisted)
    if (!id) {
      return { rows: [] }
    }
    if (i > 1) {
      queryString += ' AND '
    }
    queryString += `x.blacklisted_id=$${i}`
    queryParams.push(id)
  }
  if (queryString.length < 1) {
    raw = true
  }
  let query = `SELECT x.blacklister_name AS blacklister, x.blacklisted_name AS blacklisted FROM hafsql.blacklists x WHERE ${queryString}`
  if (raw) {
    query = 'SELECT x.blacklister_name AS blacklister, x.blacklisted_name AS blacklisted FROM hafsql.blacklists x LIMIT $1'
    queryParams.push(limit)
  }
  return pool.query(query, queryParams)
}
