export const trxIdValidator = (trxId) => {
  if (typeof trxId !== 'string') {
    return false
  }
  if (trxId.length !== 40) {
    return false
  }
  if (!/^[a-fA-F0-9]+$/.test(trxId)) {
    return false
  }
  return true
}
