export const memoValidator = (memo) => {
  if (typeof memo !== 'string') {
    return false
  }
  if (memo.length > 2047) {
    return false
  }
  for (let k = 0; k < memo.length; k++) {
    if (memo.charCodeAt(k) === 0) {
      return false
    }
  }
  return true
}
