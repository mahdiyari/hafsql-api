const MAX_START = Number.MAX_SAFE_INTEGER
export const startValidator = (start) => {
  if (typeof start !== 'number') {
    return false
  }
  if (Number.isNaN(start) || !Number.isInteger(start) || !Number.isSafeInteger(start)) {
    return false
  }
  if (start < 0) {
    return false
  }
  if (start >= MAX_START) {
    return false
  }
  return true
}
