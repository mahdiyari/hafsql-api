export const customJsonIdValidator = (id) => {
  if (typeof id !== 'string') {
    return false
  }
  if (id.length > 32) {
    return false
  }
  for (let k = 0; k < id.length; k++) {
    if (id.charCodeAt(k) === 0) {
      return false
    }
  }
  return true
}
