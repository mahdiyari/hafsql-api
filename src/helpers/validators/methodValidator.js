import { customMethods } from '../../methods/customMethods.js'
import { extraMethods } from '../../methods/extraMethods.js'
import { rpcMethods } from '../../methods/rpcMethods.js'

export const methodValidator = (method) => {
  const splitted = method.split('.')
  if (splitted.length !== 2 || splitted[0] !== 'hafsql') {
    return null
  }
  method = splitted[1]
  for (let i = 0; i < rpcMethods.length; i++) {
    if (rpcMethods[i].name === method) {
      return rpcMethods[i]
    }
  }
  for (let i = 0; i < extraMethods.length; i++) {
    if (extraMethods[i].name === method) {
      return extraMethods[i]
    }
  }
  for (let i = 0; i < customMethods.length; i++) {
    if (customMethods[i].name === method) {
      return customMethods[i]
    }
  }
  return null
}
