// uint16
export const MAX_NUM = 65535

export const customIdValidator = (id) => {
  if (typeof id !== 'number') {
    return false
  }
  if (Number.isNaN(id) || !Number.isInteger(id) || !Number.isSafeInteger(id)) {
    return false
  }
  if (id < 0) {
    return false
  }
  if (id > MAX_NUM) {
    return false
  }
  return true
}
