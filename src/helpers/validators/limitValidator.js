export const MAX_LIMIT = 100
export const DEFAULT_LIMIT = 10

export const limitValidator = (limit) => {
  if (typeof limit !== 'number') {
    return false
  }
  if (Number.isNaN(limit) || !Number.isInteger(limit) || !Number.isSafeInteger(limit)) {
    return false
  }
  if (limit < 1) {
    return false
  }
  if (limit > MAX_LIMIT) {
    return false
  }
  return true
}
