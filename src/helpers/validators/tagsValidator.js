export const tagsValidator = (tags) => {
  if (typeof tags !== 'object') {
    return false
  }
  if (!Array.isArray(tags) || tags.length < 1) {
    return false
  }
  for (let i = 0; i < tags.length; i++) {
    if (typeof tags[i] !== 'string' || tags[i].length > 24) {
      return false
    }
    for (let k = 0; k < tags[i].length; k++) {
      if (tags[i].charCodeAt(k) === 0) {
        return false
      }
    }
  }
  return true
}
