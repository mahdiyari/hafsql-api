// uint32
export const MAX_NUM = 4294967295

export const orderidValidator = (orderid) => {
  if (typeof orderid !== 'number') {
    return false
  }
  if (Number.isNaN(orderid) || !Number.isInteger(orderid) || !Number.isSafeInteger(orderid)) {
    return false
  }
  if (orderid < 0) {
    return false
  }
  if (orderid > MAX_NUM) {
    return false
  }
  return true
}
