export const timestampValidator = (timestamp) => {
  if (typeof timestamp !== 'string') {
    return false
  }
  if (timestamp.length < 10) {
    return false
  }
  const valid = Date.parse(timestamp, 'yyyy/MM/dd HH:mm:ss')
  if (Number.isNaN(valid) || valid < 1458835500000) {
    return false
  }
  return true
}
