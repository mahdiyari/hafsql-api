const validKeys = ['days', 'hours', 'minutes', 'seconds']
export const remainingCashoutValidator = (cashout) => {
  if (typeof cashout !== 'string') {
    return false
  }
  if (cashout.length < 1) {
    return false
  }
  if (!/^[a-z0-9 ]+$/.test(cashout)) {
    return false
  }
  const splitted = cashout.split(' ')
  if (splitted.length !== 2) {
    return false
  }
  if (validKeys.indexOf(splitted[1]) < 0) {
    return false
  }
  if (Number.isNaN(splitted[0])) {
    return false
  }
  return true
}
