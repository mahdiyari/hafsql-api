export const permlinkValidator = (permlink) => {
  if (typeof permlink !== 'string') {
    return false
  }
  if (permlink.length < 1 || permlink.length > 255) {
    return false
  }
  if (!/^[a-z0-9-.]*$/.test(permlink)) {
    return false
  }
  return true
}

export const parentPermlinkValidator = (permlink) => {
  if (typeof permlink !== 'string') {
    return false
  }
  if (permlink.length > 255) {
    return false
  }
  if (!/^[a-z0-9-.]*$/.test(permlink)) {
    return false
  }
  return true
}
