const MAX_INT = 2147483647
export const intValidator = (num) => {
  if (typeof num !== 'number') {
    return false
  }
  if (Number.isNaN(num) || !Number.isInteger(num) || !Number.isSafeInteger(num)) {
    return false
  }
  if (num < 0) {
    return false
  }
  if (num >= MAX_INT) {
    return false
  }
  return true
}
