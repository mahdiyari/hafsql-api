export const usernameValidator = (username) => {
  if (typeof username !== 'string') {
    return false
  }
  if (username.length < 3 || username.length > 16) {
    return false
  }
  const ref = username.split('.')
  for (let i = 0; i < ref.length; i++) {
    const label = ref[i]
    if (!/^[a-z]/.test(label)) {
      return false
    }
    if (!/^[a-z0-9-]*$/.test(label)) {
      return false
    }
    if (!/[a-z0-9]$/.test(label)) {
      return false
    }
    if (!(label.length >= 3)) {
      return false
    }
  }
  return true
}

// can be empty
export const parentAuthorValidator = (username) => {
  if (typeof username !== 'string') {
    return false
  }
  if (username.length === 0) {
    return true
  }
  if (username.length < 3 || username.length > 16) {
    return false
  }
  const ref = username.split('.')
  for (let i = 0; i < ref.length; i++) {
    const label = ref[i]
    if (!/^[a-z]/.test(label)) {
      return false
    }
    if (!/^[a-z0-9-]*$/.test(label)) {
      return false
    }
    if (!/[a-z0-9]$/.test(label)) {
      return false
    }
    if (!(label.length >= 3)) {
      return false
    }
  }
  return true
}

export const arrayUsernameValidator = (users) => {
  if (typeof users !== 'object' || !Array.isArray(users) || users.length < 1) {
    return false
  }
  for (let i = 0; i < users.length; i++) {
    if (!usernameValidator(users[i])) {
      return false
    }
  }
  return true
}
