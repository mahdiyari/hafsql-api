export const opNameValidator = (name) => {
  if (typeof name !== 'string') {
    return false
  }
  if (name.length < 1 || name.length > 255) {
    return false
  }
  if (!/^[a-z0-9_:]*$/.test(name)) {
    return false
  }
  return true
}
