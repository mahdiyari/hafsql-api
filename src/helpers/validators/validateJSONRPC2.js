import { errorObject } from '../errorObject.js'

export const validateJSONRPC2 = (req, res, next) => {
  const data = req.body
  let id = null
  if (Object.hasOwn(data, 'id') && validateId(data.id)) {
    id = data.id
  }
  if (!Object.hasOwn(data, 'jsonrpc') || !Object.hasOwn(data, 'id') || !Object.hasOwn(data, 'method')) {
    res.json(errorObject(-32600, 'Invalid Request', id))
    return
  }
  if (data.jsonrpc !== '2.0') {
    res.json(errorObject(-32600, 'Invalid Request', id))
    return
  }
  if (Object.hasOwn(data, 'params') && typeof data.params !== 'object') {
    res.json(errorObject(-32600, 'Invalid Request', id))
    return
  }
  if (typeof data.method !== 'string') {
    res.json(errorObject(-32600, 'Invalid Request', id))
    return
  }
  next()
}

const validateId = (id) => {
  if (typeof id !== 'number' && typeof id !== 'string' && id !== null) {
    return false
  }
  if (typeof id === 'number' && !Number.isInteger(id)) {
    return false
  }
  if (typeof id === 'string' && id.length > 512) {
    return false
  }
  return true
}
