import { pool } from '../database.js'
// import { validateStart } from '../validators/validateStart.js'

export const commentsByPayout = async (raw, params, callParams, limit) => {
  let queryString = ''
  const queryParams = []
  queryParams.push(limit)
  let body = false
  let metadata = false
  if (!raw) {
    const keys = Object.keys(callParams)
    let k = 0
    for (let i = 0; i < keys.length; i++) {
      if (keys[i] === 'remaining_till_cashout') {
        queryString += `AND remaining_till_cashout <= $${k + 2} `
        queryParams.push(callParams[keys[i]])
        k++
        continue
      }
      if (keys[i] === 'tags') {
        queryString += `AND tags @> $${k + 2} `
        queryParams.push(JSON.stringify(callParams[keys[i]]))
        k++
        continue
      }
      if (keys[i] === 'beneficiary') {
        queryString += `AND beneficiaries::jsonb @> $${k + 2} `
        queryParams.push(JSON.stringify([{ account: callParams[keys[i]] }]))
        k++
        continue
      }
      if (keys[i] === 'pending_payout_value') {
        queryString += `AND pending_payout_value <= $${k + 2} `
        queryParams.push(callParams[keys[i]])
        k++
        continue
      }
      if (keys[i] === 'author') {
        queryString += `AND author = $${k + 2} `
        queryParams.push(callParams[keys[i]])
        k++
        continue
      }
      if (keys[i] === 'exclude_authors') {
        queryString += `AND author != ANY($${k + 2}::text[]) `
        queryParams.push(callParams[keys[i]])
        k++
        continue
      }
      if (keys[i] === 'include_body') {
        body = callParams[keys[i]]
        continue
      }
      if (keys[i] === 'include_metadata') {
        metadata = callParams[keys[i]]
        continue
      }
    }
  }
  return await pool.query(`SELECT x.id,
    x.author,
    x.permlink,
    x.pending_payout_value,
    x.remaining_till_cashout,
    x.parent_author,
    x.parent_permlink,
    x.title,
    ${body ? 'x.body,' : ''}
    ${metadata ? 'x.json_metadata,' : ''}
    x.tags,
    x.beneficiaries,
    x.created,
    x.edited,
    x.cashout_time,
    x.last_payout,
    x.author_rewards,
    x.total_payout_value,
    x.curator_payout_value,
    x.beneficiary_payout_value,
    x.max_accepted_payout,
    x.percent_hbd,
    x.allow_votes,
    x.allow_curation_rewards 
    FROM hafsql."comments" x
    WHERE x.pending_payout_value > 0 ${queryString}
    AND deleted=false
    ORDER BY x.pending_payout_value DESC
    LIMIT $1`, queryParams)
}
