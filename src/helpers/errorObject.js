export const errorObject = (code, message, id, data = null) => {
  const error = {
    code,
    message
  }
  if (data) {
    error.data = data
  }
  return {
    jsonrpc: '2.0',
    id,
    error
  }
}
