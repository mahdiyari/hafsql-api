import { intValidator } from '../helpers/validators/intValidator.js'
import { opNameValidator } from '../helpers/validators/opNameValidator.js'
import { parentPermlinkValidator, permlinkValidator } from '../helpers/validators/permlinkValidator.js'
import { timestampValidator } from '../helpers/validators/timestampValidator.js'
import { trxIdValidator } from '../helpers/validators/trxIdValidator.js'
import { parentAuthorValidator, usernameValidator } from '../helpers/validators/usernameValidator.js'
import { startValidator } from '../helpers/validators/startValidator.js'

export const extraMethods = [
  {
    name: 'accounts',
    filters: [
      {
        param: 'name',
        validator: usernameValidator
      },
      {
        param: 'id',
        validator: intValidator
      }
    ]
  },
  {
    name: 'blocks',
    filters: [
      {
        param: 'block_num',
        validator: intValidator
      },
      {
        param: 'timestamp',
        validator: timestampValidator
      },
      {
        param: 'witness',
        validator: usernameValidator
      }
    ]
  },
  {
    name: 'transactions',
    filters: [
      {
        param: 'trx_id',
        validator: trxIdValidator
      },
      {
        param: 'block_num',
        validator: intValidator
      }
    ]
  },
  {
    name: 'operations',
    filters: [
      {
        param: 'id',
        validator: startValidator
      },
      {
        param: 'block_num',
        validator: intValidator
      },
      {
        param: 'trx_in_block',
        requires: 'block_num',
        validator: intValidator
      },
      {
        param: 'op_type_id',
        validator: intValidator
      }
    ]
  },
  {
    name: 'operation_types',
    filters: [
      {
        param: 'id',
        validator: intValidator
      },
      {
        param: 'name',
        validator: opNameValidator
      }
    ]
  },
  {
    name: 'applied_hardforks',
    filters: [
      {
        param: 'hardfork_num',
        validator: intValidator
      }
    ]
  },
  {
    name: 'dynamic_global_properties',
    filters: [
      {
        param: 'block_num',
        validator: intValidator
      }
    ]
  },
  {
    name: 'delegations',
    filters: [
      {
        param: 'delegator',
        validator: usernameValidator
      },
      {
        param: 'delegatee',
        validator: usernameValidator
      }
    ]
  },
  {
    name: 'rc_delegations',
    filters: [
      {
        param: 'delegator',
        validator: usernameValidator
      },
      {
        param: 'delegatee',
        validator: usernameValidator
      }
    ]
  },
  {
    name: 'comments',
    filters: [
      {
        param: 'author',
        validator: usernameValidator
      },
      {
        param: 'permlink',
        requires: 'author',
        validator: permlinkValidator
      },
      {
        param: 'parent_author',
        validator: parentAuthorValidator
      },
      {
        param: 'parent_permlink',
        requires: 'parent_author',
        validator: parentPermlinkValidator
      }
    ]
  },
  {
    name: 'community_subs',
    filters: [
      {
        param: 'account',
        validator: usernameValidator
      },
      {
        param: 'community',
        validator: usernameValidator
      }
    ]
  },
  {
    name: 'community_roles',
    filters: [
      {
        param: 'account',
        validator: usernameValidator
      },
      {
        param: 'community',
        validator: usernameValidator
      }
    ]
  },
  {
    name: 'blacklists',
    filters: [
      {
        param: 'blacklister',
        validator: usernameValidator
      },
      {
        param: 'blacklisted',
        validator: usernameValidator
      }
    ]
  },
  {
    name: 'mutes',
    filters: [
      {
        param: 'muter',
        validator: usernameValidator
      },
      {
        param: 'muted',
        validator: usernameValidator
      }
    ]
  },
  {
    name: 'blacklist_follows',
    filters: [
      {
        param: 'account',
        validator: usernameValidator
      },
      {
        param: 'blacklist',
        validator: usernameValidator
      }
    ]
  },
  {
    name: 'mute_follows',
    filters: [
      {
        param: 'account',
        validator: usernameValidator
      },
      {
        param: 'mute_list',
        validator: usernameValidator
      }
    ]
  },
  {
    name: 'follows',
    filters: [
      {
        param: 'follower',
        validator: usernameValidator
      },
      {
        param: 'following',
        validator: usernameValidator
      }
    ]
  },
  {
    name: 'reblogs',
    filters: [
      {
        param: 'account',
        validator: usernameValidator
      },
      {
        param: 'author',
        requires: 'permlink',
        validator: usernameValidator
      },
      {
        param: 'permlink',
        requires: 'author',
        validator: permlinkValidator
      }
    ]
  },
  {
    name: 'proposal_approvals',
    filters: [
      {
        param: 'proposal_id',
        validator: intValidator
      },
      {
        param: 'voter',
        validator: usernameValidator
      }
    ]
  }
]
