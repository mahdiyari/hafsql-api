import { boolValidator } from '../helpers/validators/boolValidator.js'
import { intValidator } from '../helpers/validators/intValidator.js'
import { remainingCashoutValidator } from '../helpers/validators/remainingCashoutValidator.js'
import { tagsValidator } from '../helpers/validators/tagsValidator.js'
import { arrayUsernameValidator, usernameValidator } from '../helpers/validators/usernameValidator.js'

export const customMethods = [
  {
    name: 'c_comments_by_payout',
    filters: [
      {
        param: 'tags',
        validator: tagsValidator
      },
      {
        param: 'beneficiary',
        validator: usernameValidator
      },
      {
        param: 'remaining_till_cashout',
        validator: remainingCashoutValidator
      },
      {
        param: 'pending_payout_value',
        validator: intValidator
      },
      {
        param: 'author',
        validator: usernameValidator
      },
      {
        param: 'exclude_authors',
        validator: arrayUsernameValidator
      },
      {
        param: 'include_body',
        validator: boolValidator
      },
      {
        param: 'include_metadata',
        validator: boolValidator
      }
    ]
  }
]
