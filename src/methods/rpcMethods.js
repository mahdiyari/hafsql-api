import { customIdValidator } from '../helpers/validators/customIdValidator.js'
import { customJsonIdValidator } from '../helpers/validators/customJsonIdValidator.js'
import { memoValidator } from '../helpers/validators/memoValidator.js'
import { orderidValidator } from '../helpers/validators/orderidValidator.js'
import { parentPermlinkValidator, permlinkValidator } from '../helpers/validators/permlinkValidator.js'
import { parentAuthorValidator, usernameValidator } from '../helpers/validators/usernameValidator.js'

const OPs = 49

/**
 * filters: allowed parameters to search an operation
 * type 'ah': parameter will be searched in hive.account_operations
 * which is the same table used by the account_history_api
 */
export const rpcMethods = [
  {
    id: 0,
    name: 'op_vote',
    filters: [
      {
        param: 'voter',
        validator: usernameValidator
      },
      {
        param: 'author',
        validator: usernameValidator
      },
      {
        param: 'permlink',
        requires: 'author',
        validator: permlinkValidator
      }
    ]
  },
  {
    id: 1,
    name: 'op_comment',
    filters: [
      {
        param: 'author',
        validator: usernameValidator
      },
      {
        param: 'permlink',
        requires: 'author',
        validator: permlinkValidator
      },
      {
        param: 'parent_author',
        validator: parentAuthorValidator
      },
      {
        param: 'parent_permlink',
        requires: 'parent_author',
        validator: parentPermlinkValidator
      }
    ]
  },
  {
    id: 2,
    name: 'op_transfer',
    filters: [
      {
        param: 'from',
        validator: usernameValidator
      },
      {
        param: 'to',
        validator: usernameValidator
      },
      {
        param: 'memo',
        validator: memoValidator
      }
    ]
  },
  {
    id: 3,
    name: 'op_transfer_to_vesting',
    filters: [
      {
        param: 'from',
        validator: usernameValidator
      },
      {
        param: 'to',
        validator: usernameValidator
      }
    ]
  },
  {
    id: 4,
    name: 'op_withdraw_vesting',
    filters: [
      {
        param: 'account',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 5,
    name: 'op_limit_order_create',
    filters: [
      {
        param: 'owner',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'orderid',
        validator: orderidValidator
      }
    ]
  },
  {
    id: 6,
    name: 'op_limit_order_cancel',
    filters: [
      {
        param: 'owner',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'orderid',
        validator: orderidValidator
      }
    ]
  },
  {
    id: 7,
    name: 'op_feed_publish',
    filters: [
      {
        param: 'publisher',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 8,
    name: 'op_convert',
    filters: [
      {
        param: 'owner',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'requestid',
        validator: orderidValidator
      }
    ]
  },
  {
    id: 9,
    name: 'op_account_create',
    filters: [
      {
        param: 'creator',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'new_account_name',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 10,
    name: 'op_account_update',
    filters: [
      {
        param: 'account',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 11,
    name: 'op_witness_update',
    filters: [
      {
        param: 'owner',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 12,
    name: 'op_account_witness_vote',
    filters: [
      {
        param: 'account',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'witness',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 13,
    name: 'op_account_witness_proxy',
    filters: [
      {
        param: 'account',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'proxy',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 14,
    name: 'op_pow',
    filters: [
      {
        param: 'worker_account',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 15,
    name: 'op_custom',
    filters: [
      // {
      //   param: 'required_posting_auths',
      //   validator: requiredAuthsValidator,
      //   type: 'ah'
      // },
      {
        param: 'id',
        validator: customIdValidator
      }
    ]
  },
  {
    id: 17,
    name: 'op_delete_comment',
    filters: [
      {
        param: 'author',
        validator: usernameValidator
      },
      {
        param: 'permlink',
        requires: 'author',
        validator: permlinkValidator
      }
    ]
  },
  {
    id: 18,
    name: 'op_custom_json',
    filters: [
      {
        param: 'id',
        validator: customJsonIdValidator
      }
    ]
  },
  {
    id: 19,
    name: 'op_comment_options',
    filters: [
      {
        param: 'author',
        validator: usernameValidator
      },
      {
        param: 'permlink',
        requires: 'author',
        validator: permlinkValidator
      }
    ]
  },
  {
    id: 20,
    name: 'op_setWithdraw_vesting_route',
    filters: [
      {
        param: 'from_account',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'to_account',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 21,
    name: 'op_limit_order_create2',
    filters: [
      {
        param: 'owner',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'orderid',
        validator: orderidValidator
      }
    ]
  },
  {
    id: 22,
    name: 'op_claim_account',
    filters: [
      {
        param: 'creator',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 23,
    name: 'op_create_claimed_account',
    filters: [
      {
        param: 'creator',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'new_account_name',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 24,
    name: 'op_request_account_recovery',
    filters: [
      {
        param: 'recovery_account',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'account_to_recover',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 25,
    name: 'op_recover_account',
    filters: [
      {
        param: 'account_to_recover',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 26,
    name: 'op_change_recovery_account',
    filters: [
      {
        param: 'account_to_recover',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'new_recovery_account',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 27,
    name: 'op_escrow_transfer',
    filters: [
      {
        param: 'from',
        validator: usernameValidator
      },
      {
        param: 'to',
        validator: usernameValidator
      }
    ]
  },
  {
    id: 28,
    name: 'op_escrow_dispute',
    filters: [
      {
        param: 'from',
        validator: usernameValidator
      },
      {
        param: 'to',
        validator: usernameValidator
      }
    ]
  },
  {
    id: 29,
    name: 'op_escrow_release',
    filters: [
      {
        param: 'from',
        validator: usernameValidator
      },
      {
        param: 'to',
        validator: usernameValidator
      }
    ]
  },
  {
    id: 30,
    name: 'op_pow2',
    filters: []
  },
  {
    id: 31,
    name: 'op_escrow_approve',
    filters: [
      {
        param: 'from',
        validator: usernameValidator
      },
      {
        param: 'to',
        validator: usernameValidator
      }
    ]
  },
  {
    id: 32,
    name: 'op_transfer_to_savings',
    filters: [
      {
        param: 'from',
        validator: usernameValidator
      },
      {
        param: 'to',
        validator: usernameValidator
      },
      {
        param: 'memo',
        validator: memoValidator
      }
    ]
  },
  {
    id: 33,
    name: 'op_transfer_from_savings',
    filters: [
      {
        param: 'from',
        validator: usernameValidator
      },
      {
        param: 'to',
        validator: usernameValidator
      },
      {
        param: 'memo',
        validator: memoValidator
      }
    ]
  },
  {
    id: 34,
    name: 'op_cancel_transfer_from_savings',
    filters: [
      {
        param: 'from',
        validator: usernameValidator
      }
    ]
  },
  {
    id: 36,
    name: 'op_decline_voting_rights',
    filters: [
      {
        param: 'account',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 39,
    name: 'op_claim_reward_balance',
    filters: [
      {
        param: 'account',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 40,
    name: 'op_delegate_vesting_shares',
    filters: [
      {
        param: 'delegator',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'delegatee',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 41,
    name: 'op_account_create_with_delegation',
    filters: [
      {
        param: 'creator',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'new_account_name',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 42,
    name: 'op_witness_set_properties',
    filters: [
      {
        param: 'owner',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 43,
    name: 'op_account_update2',
    filters: [
      {
        param: 'account',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 44,
    name: 'op_create_proposal',
    filters: [
      {
        param: 'creator',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 45,
    name: 'op_update_proposal_votes',
    filters: [
      {
        param: 'voter',
        validator: usernameValidator
      }
    ]
  },
  {
    id: 46,
    name: 'op_remove_proposal',
    filters: [
      {
        param: 'proposal_owner',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 47,
    name: 'op_update_proposal',
    filters: [
      {
        param: 'creator',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: 48,
    name: 'op_collateralized_convert',
    filters: [
      {
        param: 'requestid',
        validator: orderidValidator
      }
    ]
  },
  {
    id: 49,
    name: 'op_recurrent_transfer',
    filters: [
      {
        param: 'from',
        validator: usernameValidator
      },
      {
        param: 'to',
        validator: usernameValidator
      },
      {
        param: 'memo',
        validator: memoValidator
      }
    ]
  },

  /** ****** Virtual Operations *******/

  {
    id: OPs + 1,
    name: 'vo_fill_convert_request',
    filters: [
      {
        param: 'owner',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'requestid',
        validator: orderidValidator
      }
    ]
  },
  {
    id: OPs + 2,
    name: 'vo_author_reward',
    filters: [
      {
        param: 'author',
        validator: usernameValidator
      },
      {
        param: 'permlink',
        requires: 'author',
        validator: permlinkValidator
      }
    ]
  },
  {
    id: OPs + 3,
    name: 'vo_curation_reward',
    filters: [
      {
        param: 'curator',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: OPs + 4,
    name: 'vo_comment_reward',
    filters: [
      {
        param: 'author',
        validator: usernameValidator
      },
      {
        param: 'permlink',
        requires: 'author',
        validator: permlinkValidator
      }
    ]
  },
  {
    id: OPs + 5,
    name: 'vo_liquidity_reward',
    filters: [
      {
        param: 'owner',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: OPs + 6,
    name: 'vo_interest_operation',
    filters: [
      {
        param: 'owner',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: OPs + 7,
    name: 'vo_fill_vesting_withdraw',
    filters: [
      {
        param: 'from_account',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'to_account',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: OPs + 8,
    name: 'vo_fill_order',
    filters: [
      {
        param: 'current_owner',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'open_owner',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'current_orderid',
        validator: orderidValidator
      },
      {
        param: 'open_orderid',
        validator: orderidValidator
      }
    ]
  },
  {
    id: OPs + 9,
    name: 'vo_shutdown_witness',
    filters: [
      {
        param: 'owner',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: OPs + 10,
    name: 'vo_fill_transfer_from_savings',
    filters: [
      {
        param: 'from',
        validator: usernameValidator
      },
      {
        param: 'to',
        validator: usernameValidator
      },
      {
        param: 'memo',
        validator: memoValidator
      }
    ]
  },
  {
    id: OPs + 11,
    name: 'vo_hardfork',
    filters: []
  },
  {
    id: OPs + 12,
    name: 'vo_comment_payout_update',
    filters: [
      {
        param: 'author',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: OPs + 13,
    name: 'vo_return_vesting_delegation',
    filters: [
      {
        param: 'account',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: OPs + 14,
    name: 'vo_comment_benefactor_reward',
    filters: [
      {
        param: 'benefactor',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'author',
        requires: 'permlink',
        validator: usernameValidator
      },
      {
        param: 'permlink',
        requires: 'author',
        validator: permlinkValidator
      }
    ]
  },
  {
    id: OPs + 15,
    name: 'vo_producer_reward',
    filters: [
      {
        param: 'producer',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: OPs + 16,
    name: 'vo_clear_null_account_balance',
    filters: []
  },
  {
    id: OPs + 17,
    name: 'vo_proposal_pay',
    filters: [
      {
        param: 'receiver',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: OPs + 18,
    name: 'vo_dhf_funding',
    filters: []
  },
  {
    id: OPs + 19,
    name: 'vo_hardfork_hive',
    filters: [
      {
        param: 'account',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: OPs + 20,
    name: 'vo_hardfork_hive_restore',
    filters: [
      {
        param: 'account',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: OPs + 21,
    name: 'vo_delayed_voting',
    filters: [
      {
        param: 'voter',
        validator: usernameValidator
      }
    ]
  },
  {
    id: OPs + 22,
    name: 'vo_consolidate_treasury_balance',
    filters: []
  },
  {
    id: OPs + 23,
    name: 'vo_effective_comment_vote',
    filters: [
      {
        param: 'voter',
        validator: usernameValidator
      },
      {
        param: 'author',
        validator: usernameValidator
      },
      {
        param: 'permlink',
        requires: 'author',
        validator: permlinkValidator
      }
    ]
  },
  {
    id: OPs + 24,
    name: 'vo_ineffective_delete_comment',
    filters: []
  },
  {
    id: OPs + 25,
    name: 'vo_dhf_conversion',
    filters: []
  },
  {
    id: OPs + 26,
    name: 'vo_expired_account_notification',
    filters: [
      {
        param: 'account',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: OPs + 27,
    name: 'vo_changed_recovery_account',
    filters: [
      {
        param: 'account',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: OPs + 28,
    name: 'vo_transfer_to_vesting_completed',
    filters: [
      {
        param: 'from_account',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'to_account',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: OPs + 29,
    name: 'vo_pow_reward',
    filters: [
      {
        param: 'worker',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: OPs + 30,
    name: 'vo_vesting_shares_split',
    filters: [
      {
        param: 'owner',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: OPs + 31,
    name: 'vo_account_created',
    filters: [
      {
        param: 'new_account_name',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'creator',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: OPs + 32,
    name: 'vo_fill_collateralized_convert_request',
    filters: [
      {
        param: 'owner',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'requestid',
        validator: orderidValidator
      }
    ]
  },
  {
    id: OPs + 33,
    name: 'vo_system_warning_operation',
    filters: []
  },
  {
    id: OPs + 34,
    name: 'vo_fill_recurrent_transfer',
    filters: [
      {
        param: 'from',
        validator: usernameValidator
      },
      {
        param: 'to',
        validator: usernameValidator
      },
      {
        param: 'memo',
        validator: memoValidator
      }
    ]
  },
  {
    id: OPs + 35,
    name: 'vo_failed_recurrent_transfer',
    filters: [
      {
        param: 'from',
        validator: usernameValidator
      },
      {
        param: 'to',
        validator: usernameValidator
      },
      {
        param: 'memo',
        validator: memoValidator
      }
    ]
  },
  {
    id: OPs + 36,
    name: 'vo_limit_order_cancelled',
    filters: [
      {
        param: 'seller',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: OPs + 37,
    name: 'vo_producer_missed',
    filters: [
      {
        param: 'producer',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: OPs + 38,
    name: 'vo_proposal_fee',
    filters: [
      {
        param: 'creator',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  },
  {
    id: OPs + 39,
    name: 'vo_collateralized_convert_immediate_conversion',
    filters: [
      {
        param: 'owner',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'requestid',
        validator: orderidValidator
      }
    ]
  },
  {
    id: OPs + 40,
    name: 'vo_escrow_approved',
    filters: [
      {
        param: 'from',
        validator: usernameValidator
      },
      {
        param: 'to',
        validator: usernameValidator
      }
    ]
  },
  {
    id: OPs + 41,
    name: 'vo_escrow_rejected',
    filters: [
      {
        param: 'from',
        validator: usernameValidator
      },
      {
        param: 'to',
        validator: usernameValidator
      }
    ]
  },
  {
    id: OPs + 42,
    name: 'vo_proxy_cleared',
    filters: [
      {
        param: 'account',
        validator: usernameValidator,
        type: 'ah'
      },
      {
        param: 'proxy',
        validator: usernameValidator,
        type: 'ah'
      }
    ]
  }
]
