Note: "Required parameter" means you have to supply the required parameter too  
  
General `params`:  
- `limit` integer 1 to 100 - default 10
- `start` integer - `op_id` or `id` used for pagination
- `reverse` boolean - reverse the sorting of data - default false  
  

#### op_vote
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|voter|string|-|-|
|author|string|-|-|
|permlink|string|-|author|

#### op_comment
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|author|string|-|-|
|permlink|string|-|author|
|parent_author|string|-|-|
|parent_permlink|string|-|parent_author|

#### op_transfer
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|from|string|-|-|
|to|string|-|-|
|memo|string|-|-|

#### op_transfer_to_vesting
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|from|string|-|-|
|to|string|-|-|

#### op_withdraw_vesting
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|account|string|-|-|

#### op_limit_order_create
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|owner|string|-|-|
|orderid|number|-|-|

#### op_limit_order_cancel
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|owner|string|-|-|
|orderid|number|-|-|

#### op_feed_publish
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|publisher|string|-|-|

#### op_convert
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|owner|string|-|-|
|requestid|number|-|-|

#### op_account_create
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|creator|string|-|-|
|new_account_name|string|-|-|

#### op_account_update
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|account|string|-|-|

#### op_witness_update
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|owner|string|-|-|

#### op_account_witness_vote
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|account|string|-|-|
|witness|string|-|-|

#### op_account_witness_proxy
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|account|string|-|-|
|proxy|string|-|-|

#### op_pow
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|worker_account|string|-|-|

#### op_custom
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|id|number|-|-|

#### op_delete_comment
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|author|string|-|-|
|permlink|string|-|author|

#### op_custom_json
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|id|string|-|-|

#### op_comment_options
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|author|string|-|-|
|permlink|string|-|author|

#### op_setWithdraw_vesting_route
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|from_account|string|-|-|
|to_account|string|-|-|

#### op_limit_order_create2
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|owner|string|-|-|
|orderid|number|-|-|

#### op_claim_account
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|creator|string|-|-|

#### op_create_claimed_account
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|creator|string|-|-|
|new_account_name|string|-|-|

#### op_request_account_recovery
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|recovery_account|string|-|-|
|account_to_recover|string|-|-|

#### op_recover_account
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|account_to_recover|string|-|-|

#### op_change_recovery_account
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|account_to_recover|string|-|-|
|new_recovery_account|string|-|-|

#### op_escrow_transfer
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|from|string|-|-|
|to|string|-|-|

#### op_escrow_dispute
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|from|string|-|-|
|to|string|-|-|

#### op_escrow_release
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|from|string|-|-|
|to|string|-|-|

#### op_pow2
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|

#### op_escrow_approve
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|from|string|-|-|
|to|string|-|-|

#### op_transfer_to_savings
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|from|string|-|-|
|to|string|-|-|
|memo|string|-|-|

#### op_transfer_from_savings
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|from|string|-|-|
|to|string|-|-|
|memo|string|-|-|

#### op_cancel_transfer_from_savings
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|from|string|-|-|

#### op_decline_voting_rights
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|account|string|-|-|

#### op_claim_reward_balance
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|account|string|-|-|

#### op_delegate_vesting_shares
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|delegator|string|-|-|
|delegatee|string|-|-|

#### op_account_create_with_delegation
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|creator|string|-|-|
|new_account_name|string|-|-|

#### op_witness_set_properties
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|owner|string|-|-|

#### op_account_update2
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|account|string|-|-|

#### op_create_proposal
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|creator|string|-|-|

#### op_update_proposal_votes
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|voter|string|-|-|

#### op_remove_proposal
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|proposal_owner|string|-|-|

#### op_update_proposal
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|creator|string|-|-|

#### op_collateralized_convert
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|requestid|number|-|-|

#### op_recurrent_transfer
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|from|string|-|-|
|to|string|-|-|
|memo|string|-|-|

#### vo_fill_convert_request
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|owner|string|-|-|
|requestid|number|-|-|

#### vo_author_reward
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|author|string|-|-|
|permlink|string|-|author|

#### vo_curation_reward
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|curator|string|-|-|

#### vo_comment_reward
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|author|string|-|-|
|permlink|string|-|author|

#### vo_liquidity_reward
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|owner|string|-|-|

#### vo_interest_operation
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|owner|string|-|-|

#### vo_fill_vesting_withdraw
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|from_account|string|-|-|
|to_account|string|-|-|

#### vo_fill_order
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|current_owner|string|-|-|
|open_owner|string|-|-|
|current_orderid|string|-|-|
|open_orderid|string|-|-|

#### vo_shutdown_witness
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|owner|string|-|-|

#### vo_fill_transfer_from_savings
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|from|string|-|-|
|to|string|-|-|
|memo|string|-|-|

#### vo_hardfork
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|

#### vo_comment_payout_update
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|author|string|-|-|

#### vo_return_vesting_delegation
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|account|string|-|-|

#### vo_comment_benefactor_reward
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|benefactor|string|-|-|
|author|string|-|permlink|
|permlink|string|-|author|

#### vo_producer_reward
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|producer|string|-|-|

#### vo_clear_null_account_balance
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|

#### vo_proposal_pay
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|receiver|string|-|-|

#### vo_dhf_funding
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|

#### vo_hardfork_hive
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|account|string|-|-|

#### vo_hardfork_hive_restore
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|account|string|-|-|

#### vo_delayed_voting
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|voter|string|-|-|

#### vo_consolidate_treasury_balance
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|

#### vo_effective_comment_vote
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|voter|string|-|-|
|author|string|-|-|
|permlink|string|-|author|

#### vo_ineffective_delete_comment
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|

#### vo_dhf_conversion
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|

#### vo_expired_account_notification
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|account|string|-|-|

#### vo_changed_recovery_account
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|account|string|-|-|

#### vo_transfer_to_vesting_completed
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|from_account|string|-|-|
|to_account|string|-|-|

#### vo_pow_reward
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|worker|string|-|-|

#### vo_vesting_shares_split
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|owner|string|-|-|

#### vo_account_created
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|new_account_name|string|-|-|
|creator|string|-|-|

#### vo_fill_collateralized_convert_request
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|owner|string|-|-|
|requestid|number|-|-|

#### vo_system_warning_operation
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|

#### vo_fill_recurrent_transfer
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|from|string|-|-|
|to|string|-|-|
|memo|string|-|-|

#### vo_failed_recurrent_transfer
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|from|string|-|-|
|to|string|-|-|
|memo|string|-|-|

#### vo_limit_order_cancelled
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|seller|string|-|-|

#### vo_producer_missed
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|producer|string|-|-|

#### vo_proposal_fee
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|creator|string|-|-|

#### vo_collateralized_convert_immediate_conversion
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|owner|string|-|-|
|requestid|number|-|-|

#### vo_escrow_approved
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|from|string|-|-|
|to|string|-|-|

#### vo_escrow_rejected
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|from|string|-|-|
|to|string|-|-|

#### vo_proxy_cleared
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|account|string|-|-|
|proxy|string|-|-|
