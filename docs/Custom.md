Note: "Required parameter" means you have to supply the required parameter too  

General `params`:  
- `limit` integer 1 to 100 - default 10
- `start` integer - `op_id` or `id` used for pagination
- `reverse` boolean - reverse the sorting of data - default false  
  
  
#### c_comments_by_payout
Get the comments/posts based on their pending payout.  

|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|tags|array[string]|e.g. `["hive", "hbd"]` - comments with both `hive` AND `hbd` tag|-|
|beneficiary|string|e.g. `peakd` - comments with peakd beneficiary|-|
|remaining_till_cashout|string|`number ["days","hours","minutes","seconds"]` e.g. `1 hours` |-|
|pending_payout_value|number|integer e.g. `15` - comments lower than $15 pending payout - can be used for pagination|-|
|author|string|-|-|
|exclude_authors|array[string]|e.g. `["hbd.funder"]`- exlude posts by hbd.funder|-|
|include_body|boolean|include post body - default false|-|
|include_metadata|boolean|include post json_metadata - default false|-|
  
examples:  
```json
{"jsonrpc":"2.0", "method":"hafsql.c_comments_by_payout", "params":{"remaining_till_cashout": "1 hours", "limit": 1}, "id":1}
```
```json
{"jsonrpc":"2.0", "method":"hafsql.c_comments_by_payout", "params":{"remaining_till_cashout": "15 minutes", "limit": 1}, "id":1}
```
```json
{"jsonrpc":"2.0", "method":"hafsql.c_comments_by_payout", "params":{"pending_payout_value": 15, "limit": 1}, "id":1}
```
```json
{"jsonrpc":"2.0", "method":"hafsql.c_comments_by_payout", "params":{"tags": ["hive"], "limit": 1}, "id":1}
```
```json
{"jsonrpc":"2.0", "method":"hafsql.c_comments_by_payout", "params":{"exclude_authors": ["hbd.funder"], "limit": 1}, "id":1}
```