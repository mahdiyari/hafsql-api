Note: "Required parameter" means you have to supply the required parameter too  

General `params`:  
- `limit` integer 1 to 100 - default 10
- `start` integer - `op_id`, `id`, `block_num`, etc used for pagination
- `reverse` boolean - reverse the sorting of data - default false  
  

#### comments
Get all the comments/posts sorted DESC by default.  

|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|author|string|-|-|
|permlink|string|-|author|
|parent_author|string|-|-|
|parent_permlink|string|-|parent_author|

#### dynamic_global_properties
Get historical dynamic_global_properties at any given block number.  

|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|block_num|number|-|-|

#### accounts
Get all the accounts and their ID.  

|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|name|string|-|-|
|id|number|-|-|

#### blocks
Get all the blocks.

|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|block_num|number|-|-|
|timestamp|string|-|-|
|witness|string|-|-|

#### transactions
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|trx_id|string|-|-|
|block_num|number|-|-|

#### operations
Get all the operations.

|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|id|number|-|-|
|block_num|number|-|-|
|trx_in_block|number|-|block_num|
|op_type_id|number|-|-|

#### operation_types
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|id|number|-|-|
|name|string|-|-|

#### applied_hardforks
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|hardfork_num|number|-|-|

#### delegations
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|delegator|string|-|-|
|delegatee|string|-|-|

#### rc_delegations
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|delegator|string|-|-|
|delegatee|string|-|-|

#### community_subs
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|account|string|-|-|
|community|string|-|-|

#### community_roles
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|account|string|-|-|
|community|string|-|-|

#### blacklists
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|blacklister|string|-|-|
|blacklisted|string|-|-|

#### mutes
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|muter|string|-|-|
|muted|string|-|-|

#### blacklist_follows
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|account|string|-|-|
|blacklist|string|-|-|

#### mute_follows
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|account|string|-|-|
|mute_list|string|-|-|

#### follows
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|follower|string|-|-|
|following|string|-|-|

#### reblogs
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|account|string|-|-|
|author|string|-|permlink|
|permlink|string|-|author|

#### proposal_approvals
|Parameter|Datatype|Description|Required parameter|
|---------|--------|-----------|------------------|
|proposal_id|number|-|-|
|voter|string|-|-|